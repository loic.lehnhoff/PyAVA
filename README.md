# `PyAVA` : Python interface for the Annotation of Vocalisations in Audio recordings.

## Description

`PyAVA` is a graphical annotation tool written in Python whose purpose is to provide an easy way to draw contours on vocalisations from audio recordings. It uses [Tkinter](https://docs.python.org/3/library/tkinter.html) for its graphical interface and can be run on any OS. It does not require images (spectrogram) to function, only waveforms, which is its main advantage.

![Show PyAVA](./images/PyAVA_show.gif?raw=true)


## Features

- [x] Same tools as matplotlib plots.
- [x] Spectrogram automatically computed from waveform.
- [x] Direct modification of spectrogram included
    - Choose FFT size
    - Choose Hop length
    - Clip lowest dBFS values
    - Switch between PCEN/spectrogram
- [x] Spectrogram contour/bounding box annotations. 
- [x] Exportation of contours to local `.json` (custom format).
- [x] Possibility to move points that are already placed.
- [x] Modification of previously annotated files.

## Known issues
- Optimisation necessary when many annotations

## Requirements
- Python 3.9.7
- Packages in `requirements.txt`
- Linux / Windows / macOS (only tested with Ubuntu & Windows10 but it *should* work on any OS).


Install packages in your python environment with `$ pip install -r requirements.txt`.


## Usage

### Execution
For classic usage:
- Download PyAVA folder
- Open a terminal in the folder 
- Run `$ python PyAVA.py` in terminal.  
- Select audio file to annotate.
- Make annotations (see User actions)
- Save and quit

Run `$python PyAVA.py --help` for details and additional options.  

The annotations are saved as [JSON](http://www.json.org/) files. Each file contains a dictionnary with keys corresponding to each annotation. For each contour there is a list of points, each point is defined by a list of two elements : [time (in sec), frequency (in Hz)].

### User actions
- Use the toolbar to interact with the plot (same as with matplotlib)
- Shortcuts "p" and "w" to activate panning, "z" to activate zoom. 
- Draw lines :
    - User must not have any toolbar item selected in order to annotate the spectrogram.
    - Left-click on a name in the listbox to activate annotation with it.
    - Right-click on a name in the listbox to rename it.
    - Left-click on spectrogram to place a point from the selected category.
    - Right-click on spectrogram to remove the nearest point from the selected category.
    - Mouse wheel click on a point on spectrogram to move it around.
- Click on `Open file explorer` Button to change Wavefile used as base (will end the annotation of the current file).
- Change resolution of the spectrogram with `FFT`, `Hop length` and `clipping fields`.
- Click on `Update display` button to validate inputs.
- Click on `Switch to PCEN` to switch between PCEN and spectrogram image.

### Re-use data
#### Modification
To modify previous annotations using PyAVA interface:
- Open a terminal window in PyAVA folder 
- Run the following command:
`$python PyAVA.py --modify annotation_file audio_file`

#### Display
To display previous annotations, use the `Results` object. 
It contains several infos: coordinates of the annotations, spectrogram, waveform and more.  
- Open a terminal window in PyAVA folder 
- Open python terminal
- Run the following lines (an exemple of use):  
```python
import os
import json
from post_annotation import Results  # load functions

# load object with annotation data
annot_data = Results(os.path.join(
        ".",
        "audio_examples",
        "SCW1807_20200713_064554.wav"),
    os.path.join(
        ".",
        "outputs",
        "SCW1807_20200713_064554-contours.json"))
print(json.dumps(annot_data.coords, indent=4)) # show file content

# display annotations
annot_data.display_contours() # or annot_data.display_contours(img="pcen")
```

## Support

Please contact [me](mailto:loic.lehnhoff@gmail.com) for any question and/or idea.
