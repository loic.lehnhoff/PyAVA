# Audio-examples

These examples were taken from recordings made during the <a href="https://umr-marbec.fr/en/the-projects/dolphinfree/">DOLPHINFREE project</a>. They contain whistles of common dolphins (*Delphinus delphis*) for which annotations were necessary. `PyAVA` was designed primarily to annotate this type of sound.  
More information on how this data was collected in <a href="https://doi.org/10.3390/su142013186">*Lehnhoff et al. (2022)*</a>.