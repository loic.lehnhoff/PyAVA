##### IMPORTATIONS #####
import os
import json
from tkinter import *
from interface import FileExplorer, App
from args import fetch_inputs


##### MAIN #####
if __name__ == '__main__':
    # fetching inputs.
    audio_file_path, dir_explore, max_traj, new_sr, output, modify, initial_basename, parameters = fetch_inputs()

    if modify:
        with open(os.path.join(output, modify), "r") as f:
            coords_to_change = json.load(f)

        MainWindow = App(
            dir_explore, 
            max_traj, 
            new_sr, 
            output, 
            os.path.join(dir_explore, initial_basename),
            coords_to_change,
            parameters)

    elif len(audio_file_path)>0:
        MainWindow = App(
            dir_explore, 
            max_traj, 
            new_sr, 
            output, 
            audio_file_path,
            {},
            parameters)
    else:
        # open explorer to select first file
        groot = Tk()
        initial_file = FileExplorer(dir_explore).file
        groot.quit()
        groot.destroy()

        if len(initial_file) > 0:
            # launch UI
            MainWindow = App(
                dir_explore, 
                max_traj, 
                new_sr, 
                output, 
                initial_file,
                overwrite_parameters=parameters)
