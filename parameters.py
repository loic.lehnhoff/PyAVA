##### IMPORTATIONS #####
from screeninfo import get_monitors


##### FIXED PARAMETERS ######
_default_width = get_monitors()[0].width    # width of tk window
_default_height = get_monitors()[0].height  # height of tk window
_default_left_panel_width = 26              # Size of window's left panel
_default_nfft = 2048         				# default fft value (can be changed by user)
_default_hop_length = 512    				# default hop_length value (can be changed by user)
_default_clipping = -80      				# default clipping value in dB (can be changed by user)
_default_cmap = "viridis"   				# default cmap for spectrogram visualisation
_default_bounds = [0.05, 0.05, 0.94, 0.9]		# default boundaries for canvas in tkinter